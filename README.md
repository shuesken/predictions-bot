First, you have to create an account for your bot; I recommend matrix.org.
Next, you have to log in via the command line to get a fresh access token without encryption setup. (If you get the access token via Element, then encryption will already have been set up, yielding errors when running this bot.)

Run `curl -d '{"type":"m.login.password", "user":"yourusername", "password":"yourpassword"}' "https://matrix-client.matrix.org/_matrix/client/v3/login"`

Get the `access_token` from the response.

Copy `.env.template` to `.env` and fill in.

Now, run:

```
npm i -g typescript
npm i
npm run dev
```

The bot will automatically join invites, so use another Matrix account to invite it to a chat (and optionally others.)

Send `!market` to get the current market with a bit of formatting. Send `!rawstate` to get the raw state of the market. Send `!make bid at 40` or anything matching this regex `/!(make|cancel) (bid|offer) at (10|20|30|40|50|60|70|80|90)/` to interact with the market.
