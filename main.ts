import 'dotenv/config'
import {
  MatrixClient,
  MessageEvent,
  RustSdkCryptoStorageProvider,
  SimpleFsStorageProvider,
  AutojoinRoomsMixin,
} from 'matrix-bot-sdk'

import { deriveMarket, prices } from './market.js'

// SET UP CLIENT AND CRYPTO
const crypto = new RustSdkCryptoStorageProvider('bot_sled')
const homeserverUrl = process.env.HOMESERVER_URL
const username = process.env.USERNAME
const accessToken = process.env.ACCESS_TOKEN

if (!homeserverUrl || !accessToken || !username) {
  console.error('specify homeserver url and access token')
  process.exit(1)
}

const storage = new SimpleFsStorageProvider('bot.json')
const client = new MatrixClient(homeserverUrl, accessToken, storage, crypto)
AutojoinRoomsMixin.setupOnClient(client)

const joinedRooms = await client.getJoinedRooms()
await client.crypto.prepare(joinedRooms)

// SET UP MESSAGE HANDLING
const events: any[] = []
const re = /!(make|cancel) (bid|offer) at (10|20|30|40|50|60|70|80|90)/

client.on('room.message', async (roomId: string, event: any) => {
  const message = new MessageEvent(event)

  if (message.messageType !== 'm.text') {
    console.log('ignoring message because it is not text:', message.messageType)
    return
  }

  if (re.test(message.textBody)) {
    console.log('added new market event: ', message.textBody)
    events.push(event)
  } else if (message.textBody === '!market') {
    await client.replyNotice(roomId, event, formattedMarket())
  } else if (message.textBody === '!rawstate') {
    await client.replyNotice(roomId, event, rawState())
  }
})

client.start().then(() => console.log('Client started!'))

// HELPER FUNCTIONS
function rawState() {
  const timeline = events.map(transformEventToOrderOrCancellation)
  const clearedMarket = deriveMarket(timeline)
  return JSON.stringify(clearedMarket, null, 2)
}
function formattedMarket() {
  const timeline = events.map(transformEventToOrderOrCancellation)
  const clearedMarket = deriveMarket(timeline)
  let formattedMarket = ''

  prices.forEach((price) => {
    const amount = clearedMarket.outstandingOrders[price].length
    const isBid = clearedMarket.outstandingOrders[price][0]?.isBid ?? null
    const line = `${price}: ${amount} ${
      amount > 0 ? (isBid ? 'bids' : 'offers') : ''
    }\n`
    formattedMarket += line
  })

  return formattedMarket
}

function transformEventToOrderOrCancellation(event: any) {
  const message = new MessageEvent(event)
  const match = message.textBody.match(re)
  if (!match) throw Error('bad event')
  const isCancellation = match[1] == 'cancel'
  const isBid = match[2] == 'bid'
  const price = parseInt(match[3]) as Price
  const uuid = message.eventId
  const userId = message.sender

  const orderOrCancellation = {
    uuid,
    userId,
    price,
    isCancellation,
    isBid,
  }

  return orderOrCancellation
}

export {} // needed to make top-level await work
